<script type="text/javascript">

const url = 'https://api.capturedata.ie​';

/*

flightModel should include the following attributes
but I was unable to extract these from the website

origin_airport
destination_airport
flight_number
flight_price
flyout_date
flyout_time
flight_duration

*/

var flightModel = {
	flight_info: document.getElementsByTagName('flight-card')[0].innerText,
	flight_number: document.getElementsByTagName('flight-card')[0].dataset.ref,
	flight_price: document.getElementsByTagName('flight-price')[0].textContent
};


fetch(url, { method: 'POST', headers: { 'Content-Type': 'application/json'}, body: flightModel })
	.then(results => results.json())
	.then(console.log);


</script>
