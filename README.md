# Boxever Technical Test #

### Part 1 - JS Script & Test Plan ###

* :white_check_mark: JS Script: `ryanair_script.js` (unable to capture data for detailed flight model)
* :white_check_mark: Test Plan: `test_plan_ryanair.pdf`

### Part 2 - Python Script & CSV Input ###

* :white_check_mark: Python Script: `extract_transform.py`
* :white_check_mark: Input Data CSV: `input_data.csv`

### Part 3 - Bitbucket Repo ###

* :white_check_mark: Pushed to Bitbucket Repoistory

